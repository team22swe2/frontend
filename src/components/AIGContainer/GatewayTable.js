import React from "react";
import ReactTable from "react-table";
import auth from "../common/auth";
import { Button, Modal, Form } from "react-bootstrap";

class GatewayTable extends React.Component {
  constructor() {
    super();

    this.state = {
      isModalShowing: false,
      tableData: [
        {
          gwID: "",
          Owner: ""
        }
      ]
    };
  }

  componentDidMount = async () => {
    let gateways = await this.fetchOwnedGateways();
    this.setState({ tableData: gateways });
  };

  handleModalClose = async () => {
    let gateways = await this.fetchOwnedGateways();
    this.setState({ isModalShowing: false, tableData: gateways });
  };

  fetchOwnedGateways = async () => {
    let ownedGatewaysResp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/owned_gateways",
      {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      }
    );
    let gateways = await ownedGatewaysResp.json();
    return gateways;
  };

  handleModalOpen = () => {
    this.setState({ isModalShowing: true });
  };

  handleSubmit = async event => {
    event.preventDefault();
    let gwID = event.target.gwID.value;
    let resp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/new_gateway/?gwID=" +
        gwID,
      {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      }
    );
  };

  render() {
    const { tableData } = this.state;

    return (
      <>
        <Button variant="primary" onClick={this.handleModalOpen}>
          Add Gateway
        </Button>
        <ReactTable
          data={tableData}
          columns={[
            {
              Header: "Gateway Name",
              accessor: "gwID"
            },
            {
              Header: "Owner",
              accessor: "owner"
            }
          ]}
          defaultPageSize={30}
          className="-striped -highlight"
        />
        <Modal
          show={this.state.isModalShowing}
          onHide={this.handleModalClose}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Gateway</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="gatewayName">
                <Form.Label>Gateway Name</Form.Label>
                <Form.Control
                  name="gwID"
                  placeholder="Enter Gateway Name"
                  required
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Add Gateway
              </Button>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default GatewayTable;
