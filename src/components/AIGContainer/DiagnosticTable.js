import React from "react";
import ReactTable from "react-table";
import auth from "../common/auth";
import { Button, Modal, Form } from "react-bootstrap";
import * as Datetime from "react-datetime";
import * as moment from "moment";

class DiagnosticTable extends React.Component {
  constructor() {
    super();

    this.state = {
      date: null,
      isModalShowing: false,
      gateways: [],
      tableData: [
        {
          gwID: "",
          timestampStart: "",
          timestampEnd: "",
          testType: "",
          testResult: "",
          errorReport: "",
          elapsedTime: "",
          __v: ""
        }
      ]
    };
  }

  componentDidMount = async () => {
    let diagnostics = await this.fetchDiagnostics();
    let gateways = await this.fetchGateways();
    this.setState({ tableData: diagnostics, gateways: gateways });
  };

  handleModalClose = async () => {
    this.setState({ isModalShowing: false, date: null });
  };

  handleRefresh = async () => {
    let diagnostics = await this.fetchDiagnostics();
    this.setState({ tableData: diagnostics });
  };

  fetchDiagnostics = async () => {
    let diagResp = await fetch(
      "https://team22.softwareengineeringii.com/api/cic/diagnostic",
      {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      }
    );
    let diagnostics = await diagResp.json();
    return diagnostics;
  };
  fetchGateways = async () => {
    let gatewayResp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/owned_gateways",
      {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      }
    );
    let gateways = await gatewayResp.json();
    return gateways;
  };

  handleModalOpen = () => {
    this.setState({ isModalShowing: true });
  };

  getTestTypes = selectedOptionsArr => {
    let testTypes = [];
    for (var i = 0; i < selectedOptionsArr.length; i++) {
      testTypes.push(selectedOptionsArr[i].id);
    }
    return testTypes;
  };

  handleDateChange = date => {
    let isoDateString = moment(date).toISOString();
    this.setState({ date: isoDateString });
  };

  handleSubmit = async event => {
    event.preventDefault();
    let name = event.target.gwID.value;
    let testQueue = this.getTestTypes(event.target.testTypes.selectedOptions);
    let postBody = {
      gwID: name,
      test_queue: testQueue
    };
    if (this.state.date) {
      postBody["time_to_run"] = this.state.date;
    }
    console.log(postBody);
    let resp = await fetch(
      "https://team22.softwareengineeringii.com/api/cic/order_tests",
      {
        method: "POST",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken(),
          "Content-Type": "application/json"
        },
        body: JSON.stringify(postBody)
      }
    );
  };

  render() {
    const { tableData } = this.state;

    return (
      <>
        <Button variant="primary" onClick={this.handleModalOpen}>
          Schedule Diagnostic
        </Button>
        <Button variant="primary" onClick={this.handleRefresh}>
          Refresh Diagnostics
        </Button>
        <ReactTable
          data={tableData}
          columns={[
            {
              Header: "Gateway ID",
              accessor: "gwID"
            },
            {
              Header: "Time Start",
              accessor: "timestampStart"
            },
            {
              Header: "Time End",
              accessor: "timestampEnd"
            },
            {
              Header: "Test Type",
              accessor: "testType"
            },
            {
              Header: "Test Result",
              accessor: "testResult"
            },
            {
              Header: "Error Report",
              accessor: "errorReport"
            },
            {
              Header: "Elapsed Time",
              accessor: "elapsedTime"
            }
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <Modal
          show={this.state.isModalShowing}
          onHide={this.handleModalClose}
          animation={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Schedule Diagnostic</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="gatewayName">
                <Form.Label>Gateway Name</Form.Label>
                {/* <Form.Control
                  name="gwID"
                  placeholder="Enter Gateway Name"
                  required
                /> */}
                <Form.Control as="select" name="gwID" required>
                  {this.state.gateways.map(gw => {
                    return <option id={gw._id}>{gw.gwID}</option>;
                  })}
                </Form.Control>
                <Form.Label>Test Type</Form.Label>
                <Form.Control as="select" multiple name="testTypes" required>
                  <option id="memory_test">Memory Test</option>
                  <option id="floating_test">Floating Test</option>
                  <option id="storage_test">Storage Test</option>
                  <option id="integer_test">Integer Test</option>
                  <option id="prime_test">Prime Test</option>
                </Form.Control>
                <Form.Label>Time</Form.Label>
                <Datetime
                  inputProps={{
                    name: "timeToRun",
                    placeholder: "Leave blank for test to run immediately"
                  }}
                  onBlur={this.handleDateChange}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Run Diagnostic
              </Button>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
export default DiagnosticTable;
