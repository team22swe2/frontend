import React from "react";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import socketIOClient from "socket.io-client";

class NotificationsExample extends React.Component {
  constructor() {
    super();
    this.state = {
      socket: null
    };
  }
  componentDidMount() {
    let socket = socketIOClient("https://team22.softwareengineeringii.com");
    socket.on("connect", function() {
      console.log("connected!");
      // socket.emit("greet", { message: "Hello Mr.Server!" });
    });

    // socket.on("respond", function(data) {
    //   let dataStr = JSON.stringify(data);
    //   NotificationManager.info(`ws message: ${dataStr}`);
    // });

    socket.on("isSpeeding", function(data) {
      console.log(JSON.stringify(data));
      let dataStr = JSON.stringify(data);
      NotificationManager.info(`${dataStr}`);
    });

    this.setState({ socket: socket });
  }

  componentWillUnmount() {
    this.state.socket.emit("disconnect");
  }

  createNotification = type => {
    return () => {
      switch (type) {
        case "info":
          NotificationManager.info("Diagnostic request sent");
          break;
        case "success":
          NotificationManager.success(
            "Diagnostic complete",
            "Click here for results"
          );
          break;
        case "warning":
          NotificationManager.warning(
            "Warning message",
            "Close after 3000ms",
            3000
          );
          break;
        case "error":
          NotificationManager.error("Error message", "Click me!", 5000, () => {
            alert("callback");
          });
          break;
      }
    };
  };

  render() {
    return (
      <div>
        <p>Listening for notifications...</p>

        <NotificationContainer />
      </div>
    );
  }
}

export default NotificationsExample;
