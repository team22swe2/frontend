import React from "react";
import { NavigationBar } from "./NavigationBar";
import FetchHeartBeat from "./FetchHeartBeat";
import TableauReact from "./TableauReactTest";
import DiagnosticTable from "./DiagnosticTable";
import GatewayTable from "./GatewayTable";
import { ProtectedRoute } from "../common/protected-route";
import Logout from "./Logout";
import NotificationToast from "./Notifications";
import { Route } from "react-router-dom";
export const AIGContainer = () => (
  <div>
    <div className="container">
      <NavigationBar />
      <ProtectedRoute exact path="/" component={TableauReact} />
      <ProtectedRoute exact path="/diagnostics" component={DiagnosticTable} />
      <ProtectedRoute exact path="/gateways" component={GatewayTable} />
      <ProtectedRoute
        exact
        path="/notifications"
        component={NotificationToast}
      />
      {/* <ProtectedRoute exact path="/heartbeat" component={FetchHeartBeat} /> */}
      <ProtectedRoute exact path="/logout" component={Logout} />
    </div>
  </div>
);
