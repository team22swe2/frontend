import React from "react";
import axios from "axios";
import ReactTable from "react-table";

class FetchHeartBeat extends React.Component {
  constructor() {
    super();

    this.state = {
      tableData: [
        {
          _id: "",
          heartbeat: "",
          __v: ""
        }
      ]
    };
  }

  componentDidMount() {
    axios
      .get("https://team22.softwareengineeringii.com/api/cic/heartbeat", {
        responseType: "json"
      })
      .then(response => {
        this.setState({ tableData: response.data });
      });
  }

  render() {
    const { tableData } = this.state;

    // ReactTable.default necessary for browser env example

    return (
      <ReactTable
        data={tableData}
        columns={[
          {
            Header: "HEARTBEATS:",
            columns: [
              {
                Header: "ID:",
                accessor: "_id"
              }
            ]
          },
          {
            Header: "",
            columns: [
              {
                Header: "HeartBeat",
                accessor: "heartbeat"
              }
            ]
          },
          {
            Header: "",
            columns: [
              {
                Header: "Version",
                accessor: "__v"
              }
            ]
          }
        ]}
        defaultPageSize={10}
        className="-striped -highlight"
      />
    );
  }
}

export default FetchHeartBeat;
