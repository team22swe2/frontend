import React from "react";
import { Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";

class RegisterPage extends React.Component {
  state = {
    isUserValidated: false
  };
  handleSubmit = async event => {
    event.preventDefault();
    let name = event.target.name.value;
    let email = event.target.email.value;
    let password = event.target.password.value;
    let postBody = {
      name: name,
      email: email,
      password: password
    };
    let validResp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/register",
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(postBody)
      }
    );
    if (validResp.status === 201) {
      this.setState({ isUserValidated: true });
    }
    let validRespJson = await validResp.json();
    console.log(validRespJson);
  };

  render() {
    if (this.state.isUserValidated) {
      return <Redirect to="/login" />;
    } else {
      return (
        <>
          <h1>AIG Catastrophe Management</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Name</Form.Label>
              <Form.Control name="name" placeholder="Enter name" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" placeholder="Enter email" />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                name="password"
                type="password"
                placeholder="Enter password"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Sign up
            </Button>
          </Form>
          <Link to="/login">Already a user? Click here to login </Link>
        </>
      );
    }
  }
}

export default RegisterPage;
