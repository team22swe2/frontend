import React from "react";
import { Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import auth from "../common/auth";
class LoginPage extends React.Component {
  state = {
    isUserValidated: false
  };
  handleSubmit = async event => {
    event.preventDefault();
    let email = event.target.email.value;
    let password = event.target.password.value;
    let postBody = {
      email: email,
      password: password
    };
    let validResp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/login",
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(postBody)
      }
    );
    console.log(validResp.status);
    if (validResp.status === 200) {
      let json = await validResp.json();
      auth.login(json.token);
      this.setState({ isUserValidated: true });
    }
  };

  render() {
    if (this.state.isUserValidated) {
      return <Redirect to="/" />;
    } else {
      return (
        <>
          <h1>AIG Catastrophe Management</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" placeholder="Enter email" />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                name="password"
                type="password"
                placeholder="Enter password"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Log in
            </Button>
          </Form>
          <Link to="/register">New? Click here to register </Link>
        </>
      );
    }
  }
}

export default LoginPage;
