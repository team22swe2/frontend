import React from "react";
import Login from "./Login";
import Register from "./Register";
import { Route, Redirect } from "react-router-dom";
export const LoginContainer = () => (
  <div className="container">
    <Route exact path="/" render={() => <Redirect to="/login" />} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
  </div>
);
