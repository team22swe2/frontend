import React from "react";
import auth from "../common/auth";
import { Redirect } from "react-router-dom";
export default class Logout extends React.Component {
  state = {
    hasLoggedOut: false
  };
  async componentDidMount() {
    let logoutResp = await fetch(
      "https://team22.softwareengineeringii.com/api/accountservices/deactivatetokens",
      {
        method: "POST",
        credentials: "include",
        headers: {
          Authorization: "Bearer " + auth.getToken()
        }
      }
    );
    if (logoutResp.status === 200) {
      this.setState({ hasLoggedOut: true });
    }
  }
  render() {
    if (this.state.hasLoggedOut) {
      return <Redirect to="/login" />;
    } else {
      return "logged out";
    }
  }
}
