//https://www.youtube.com/watch?v=Y0-qdp-XBJg
//https://stackoverflow.com/questions/49788580/how-to-redirect-to-correct-client-route-after-social-auth-with-passport-react
class Auth {
  constructor() {
    this.authenticated = false;
    this.token = "";
  }

  login(token) {
    this.authenticated = true;
    this.token = token;
  }

  logout() {
    this.authenticated = false;
    this.token = "";
  }
  isAuthenticated() {
    return this.authenticated;
  }
  getToken() {
    return this.token;
  }
}

export default new Auth();
