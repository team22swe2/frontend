import React from "react";
import tableau from "tableau-api";
import Iframe from "react-iframe";
//https://community.tableau.com/thread/266182
//https://cmtoomey.github.io/category/2017/05/23/reacttableau.html
//https://community.tableau.com/thread/157585
class TableauTest extends React.Component {
  componentDidMount() {
    this.initTableau();
  }
  initTableau() {
    const vizUrl =
      "https://public.tableau.com/views/AIGFinal/Dashboard1?:display_count=y&:origin=viz_share_link";
    const options = {
      width: window.outerWidth - 100,
      height: window.outerHeight - 100,
      hideTabs: true,
      hideToolbar: false
    };
    const vizContainer = this.vizContainer;
    let viz = new window.tableau.Viz(vizContainer, vizUrl, options);
  }
  render() {
    return (
      <div
        ref={div => {
          this.vizContainer = div;
        }}
      ></div>
    );
  }
}

export default TableauTest;
