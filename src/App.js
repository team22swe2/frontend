import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Layout } from "./components/Layout";
import { LoginContainer } from "./components/LoginContainer/LoginContainer";
import { AIGContainer } from "./components/AIGContainer/AIGContainer";
import "./App.css";
import "react-table/react-table.css";
import "./styles.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/(login|register)" component={LoginContainer} />
            <Route component={AIGContainer} />
          </Switch>
        </Layout>
      </Router>
    </div>
  );
}

export default App;
